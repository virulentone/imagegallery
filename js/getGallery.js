(function(document, window) {
/*
* Function: getJSON
* - Query to get the Flickr data feed
* - Returned as JSON
* @param 1: url of feed
*/ 


//with paged items
var pageToSee = 1;

//URI
var photoURI = 'https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=e6a70807712966abf004bfaac1b82d5d&photoset_id=72157631594286671&extras=original_format&per_page=16&media=photos&format=json&page=' + pageToSee;

getJSON(photoURI);

function getJSON(url){
var script = document.createElement('script');
  script.type = 'application/javascript';
  script.src = url;
  var head = document.head;
  document.getElementsByTagName('head')[0].appendChild(script); 
  document.getElementsByTagName('head')[0].removeChild(script); 
}

//JSONP callback
function jsonFlickrApi(data){
  displayGallery(data); //on success display gallery
  buildPager(data); //build out pager for gallery
  lightboxInit(); //add event handler for lightbox 
}
window.jsonFlickrApi = jsonFlickrApi;



/*
* Function: displayGallery
* - Display the gallery of images based on the returned JSON feed
* @para returned data: JSON obj
*/
var displayGallery = function(data){

  for (i = 0; i < data.photoset.photo.length; i++) {

    //image vars
    var farm = data.photoset.photo[i]['farm'];
    var server = data.photoset.photo[i]['server'];
    var id = data.photoset.photo[i]['id'];
    var secret = data.photoset.photo[i]['secret'];

  //build image url 
    var img_src = "http://farm" + farm + ".static.flickr.com/" + server + "/" + id + "_" + secret + "_m.jpg";

    var img_tag = '<img src="' + img_src + '"/> ';    

  //create new list item
    var thumb = document.createElement("li"); 

  //add a class to the li tag
    thumb.className = 'thumbnails-list-img';
 
  //add the image to the list item
    thumb.innerHTML = img_tag;   
    
    //add the image title as a data attribute
    thumb.getElementsByTagName('IMG')[0].setAttribute("data-title", data.photoset.photo[i]['title']);
 
  //add the new list item to the list
    var eleThumbsWrapper = document.getElementById("js-thumbnails-list");
    eleThumbsWrapper.appendChild(thumb);    
 
 }   
 
}//displayGallery


/*
* Function: buildPager
* - Build out the pager based on number of images, 16 images per page
* - Add event listener to pager links
* @para returned data: JSON obj
*/
function buildPager(data){

 pages = data.photoset.pages;

 //add event listers to pager buttons
 var pagerList = document.getElementById("js-pager"); 

 //if the pager isn't already created
 if (pagerList.children.length == 0){  
  for (i = 1; i <= pages; i++) {   
   //create new list item
   var page = document.createElement("li");
   page.innerHTML = '<a href="#" class="paged" id="page-' + i + '">' + i + '</a>'
   var elePageLink = document.getElementById("js-pager");
   elePageLink.appendChild(page);         
  }

  //add class to first page
  if (document.getElementById('page-1')){
    document.getElementById('page-1').className += ' active';
  }

   pagerList.addEventListener('click', function (event) {  
    if (event.target.href) { //if link exists
     var pageID = event.target.id;
     var queryPage = pageID.substring(5);
    
     //when link is clicked pass the page number (queryPage) to the newPage function to get the new page
     closeLB();
     newPage(queryPage);  
         
    }
   }, false); 
    
 }//if the pager isn't already created 
    
}//buildpager
  


/*
* Function: newPage
* - Update the selection of images, i.e. display new page
* - Removes existing data on success 
* - Updates query with new page number, call query and re-displays data
* @param pageToSee: page number to load
*/
var newPage = function(pageToSee){

 //remove existing images
 var imgParent = document.getElementById("js-thumbnails-list");
 var imgChild = document.getElementsByClassName("thumbnails-list-img");

 //if thumbnail images exist

  while(imgChild.length) { //use while loop at index 0 as after each item removed everything shuffle's down one so always have a index 0 item until they are all removed
   imgParent.removeChild(imgChild[0]); 
  }
 
  var currentActive = document.getElementsByClassName('active')[0];
 
  if(currentActive)
   currentActive = currentActive.id;

  //remove current 'active' class    
  if (document.getElementById(currentActive)) {
   document.getElementById(currentActive).className =
    document.getElementById(currentActive).className.replace
      ( /(?:^|\s)active(?!\S)/g , '' )           
  } 
  
  //set new active class
  newActive = document.getElementById('page-' + pageToSee);
  newActive.className += ' active';
  
  var photoURI = 'https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=e6a70807712966abf004bfaac1b82d5d&photoset_id=72157631594286671&extras=original_format&per_page=16&media=photos&format=json&page=' + pageToSee; 
  getJSON(photoURI);



}//newPage 


})(document, window);