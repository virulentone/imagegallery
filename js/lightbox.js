/*
 * Function: lightboxInit
 * - Add event listeners to all images 
 * - Get image src for clicked on image
 * - launch lightbox with clicked image
 */  
function lightboxInit() {
  //add event listers to images
  var thumbnail = document.getElementById("js-thumbnails-list");      
  thumbnail.addEventListener('click', function (event) {       
        
    if (event.target.tagName == "IMG") { //if link exists 
      
      //add active class to the li of the img
      event.target.parentElement.className += ' active';
      
      //get image src and title       
      imgSrc = event.target.src;   
      imgTitle = event.target.attributes[1].nodeValue;       
      
      //launch the lightbox 
      launchBox(imgSrc, imgTitle);                         
    }
    
  }, false);
  
  //add first and last class to list of thumbnails
  var thumbList = thumbnail.getElementsByTagName("LI");
  thumbList[0].className += ' first';
  thumbList[thumbList.length - 1].className += ' last';
     
} 
window.lightboxInit = lightboxInit;    

 
/*
 * Function: launchBox
 * - Create img tag and add the source
 * - Wrap in hidden div
 * - Make div visible
 * - Call lightbox btn handler function 
 * @param - image source
 * @param - image title 
 */
   
function launchBox(imgSrc, imgTitle) {
  //make image tag
  var thumbImg = '<img src="' + imgSrc + '"/>';
  
  //make title tag (span)
  var titleS = document.createElement('SPAN');
  titleS.innerHTML = imgTitle;  
  
  //place in lb-wrapper div
  var lbWrapper = document.getElementById('lb-wrapper');
  lbWrapper.innerHTML = thumbImg;  
  lbWrapper.appendChild(titleS);  
  
  //make the container visible 
  var lbC = document.getElementById('lb-container');
  lbC.style.display = "block";
  
  //Left / right btn handling 
  btnClick(); 
  
  document.onkeydown = function(event) {
      var c = event.which || event.keyCode;
      if (c == 27) //ecp key
        closeLB();
      
      if (c == 39) //right arrow key
        scroll('right');

      if (c == 37) //right arrow key
        scroll('left');
       
  };  
  
  //close when click on close btn
  closeArrow = document.getElementById('close-btn');  
  if (closeArrow) {
    closeArrow.onclick = function (e) {
      closeLB();  
    }
  }
  
  //close when click outside of image area 
  lbC.onclick = function (e) {       
    if (e.target.nodeName == 'DIV' && e.target.id == 'lb-container')
      closeLB();               
  }
    
}


/*
 * Function: btnClick
 * - Check if the left or right btn was clicked 
 * - Call Scroll image function accordingly 
 */
function btnClick(){
  
  var btnClicked = '';
  
  document.getElementById('left-btn').onclick = function (event) {
    btnClicked = 'left';
    if (btnClicked != undefined)
      scroll(btnClicked); 
  };     
  
  document.getElementById('right-btn').onclick = function (event) {
    btnClicked = 'right';
    if (btnClicked != undefined)
      scroll(btnClicked);
  };    
  
}


/*
 * Function: scroll
 * - Get current image index, remove active class
 * - Determine direction and/or loop
 * - Switch image and add active class  
 * - Display active image in launchBox()
 * @param - scroll direction 
 */
function scroll(direction){  
  
  //index of current image
  var imgList = Array.prototype.slice.call( document.getElementById('js-thumbnails-list').children ); //array of 16 img objects
  var liRef = document.getElementsByClassName('active')[0];
  var activeIndex = imgList.indexOf( liRef );    
  var dirSign = 1;
       
  //remove 'active' from imgList[activeIndex]    
  if (imgList[activeIndex]) {  
  imgList[activeIndex].className =
    imgList[activeIndex].className.replace
      ( /(?:^|\s)active(?!\S)/g , '' )      
  } 
   
  if (direction === 'right' && activeIndex != imgList.length - 1 ){ //(imgList.length - 1) is 15 (last item)        
    dirSign = 1;
    regScroll();   
  }else if (direction === 'right' && activeIndex == imgList.length - 1 ) { //not equal to last item
    loopRight();
  }else if(direction === 'left' && activeIndex != 0 ){ //not equal to first item
    dirSign = -1;
    regScroll();
  }else if (direction === 'left' && activeIndex == 0 ) {
    loopLeft();    
  } 
  
  /*
   * Scroll left or right
   */
  function regScroll(){
    //set 'active' to imgList[activeIndex + 1]
    imgList[activeIndex + dirSign].className += ' active';
    
    //call launchBox(imgSrc, imgTitle) and pass in src & title for imgList[activeIndex + 1] 
    newSrc = imgList[activeIndex + dirSign].children[0].src; 
    newTitle = imgList[activeIndex + dirSign].children[0].attributes[1].nodeValue;   
    launchBox(newSrc, newTitle);      
  }

  /*
   * Loop right
   */  
  function loopRight(){
    //set 'active' to imgList[0]
    imgList[0].className += ' active';
    
    //call launchBox(imgSrc) and pass in src for imgList[activeIndex + 1] 
    newSrc = imgList[0].children[0].src;    
    newTitle = imgList[0].children[0].attributes[1].nodeValue;   
    launchBox(newSrc, newTitle);
  }

  /*
   * Loop left 
   */    
  function loopLeft(){
    //set 'active' to last item in imgList    
    imgList[imgList.length - 1].className += ' active';
    
    //call launchBox(imgSrc) and pass in src for last item in imgList[] 
    newSrc = imgList[imgList.length - 1].children[0].src;    
    newTitle = imgList[imgList.length - 1].children[0].attributes[1].nodeValue;    
    launchBox(newSrc, newTitle);    
  }
  
    
}


/*
 * Function: closeLB
 * - Remove active class 
 * - Remove image from #lb-wrapper
 * - Change #lb-container back to display: none    
 */
function closeLB(){
  
  //Remove active class
  //index of current image
  var imgList = Array.prototype.slice.call( document.getElementById('js-thumbnails-list').children ); //array of 16 img objects
  var liRef = document.getElementsByClassName('active')[0];
  var activeIndex = imgList.indexOf( liRef );     

  //remove 'active' from imgList[activeIndex]      ****Do a check here  
  if (imgList[activeIndex]) {
    imgList[activeIndex].className =
      imgList[activeIndex].className.replace
        ( /(?:^|\s)active(?!\S)/g , '' )    
  } 
   

  //Remove image from #lb-wrapper
  lbImgWrap = document.getElementById('lb-wrapper');  
  lbImg = lbImgWrap.getElementsByTagName('IMG')[0];
  if (lbImg)
    lbImgWrap.removeChild(lbImg);

  //Change #lb-container back to display: none
      
  //make the container hidden 
  var lbC = document.getElementById('lb-container');
  lbC.style.display = "none";       
  
}

window.closeLB = closeLB;