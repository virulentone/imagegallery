#Flickr API Image Gallery

##Author
Vik Kumar

##File List
* index.html
* css/style.min.css
* js/getgallery.min.js
* js/lightbox.min.js
* images/sprite-3.png  
Note: original CSS & JS files have also been left in their respective directories for reviewing easily. 

##About
This project is a technical exercise from Slack. This project had four main goals:

* The ability to access a public API and successfully retrieve data from it
* The ability to display that data on a page
* The ability to update the UI of a page without refreshing
* The ability to do all of the above using only native JavaScript (no libraries such as jQuery)

Along with the image the title of each image is also pulled from the public API and both the image and title are shown in a lightbox.

##Technical Details

###API
The public API used was from Flickr: https://www.flickr.com/services/api/flickr.photosets.getPhotos.html

The specific data feed can be found here: https://www.flickr.com/photos/ricepot/sets/72157631594286671/with/8011522466/

The data was retrieved using JSONP. The considerations that went into this included:
* initially XMLHttpRequest was employed with data being retrieved through JSON 
* however to support legacy browsers (ex. IE 9) XMLHttpRequest/XDomainRequest were not a valid option due to CORS and also given that XDomainRequest requires the same HTTP/HTTPS scheme to match between client and page hosting the API; as the Flickr API sits behind https scheme and my site behind http scheme the only remaining option was JSONP.

The images retrieved were broken into 16 images per page and pagination is included to see additional data (which retrieve new set of data without refreshing the page)

###Browser Support
* Browser support includes latest versions of all major browsers (Chrome, Firefox, Safari, IE) plus up to two version back 
* The website has been developed to be responsive and will adapt to the users screen

##Usage

The website will load a set of images obtained from a remote Flickr feed and display them in a grid. Then any image can be viewed through a lightbox. Website usage is as follows: 

* Open lightbox: click on an image to launch the lightbox
* Navigate: use right or left arrows on your keyboard or the left/right arrows in the lightbox
* Close lightbox: click the X at the top right of the image or hit the Escape key or click off the image
* Change page: click a page number link at the bottom of the page to retrieve more data which will display new pictures without refreshing the page